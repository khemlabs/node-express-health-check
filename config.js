const port =
	process.env.STATUS_REQUEST_PORT || process.env.PORT
		? `:${process.env.STATUS_REQUEST_PORT || process.env.PORT}`
		: null;

const getBaseURL = () => {
	if (process.env.STATUS_REQUEST_BASE_URL) {
		return process.env.STATUS_REQUEST_BASE_URL.indexOf(':') > 0
			? process.env.STATUS_REQUEST_BASE_URL
			: `${process.env.STATUS_REQUEST_BASE_URL}${port}`;
	}
	return `localhost${port}`;
};

module.exports = {
	request: {
		config: {
			baseURL: getBaseURL(),
			url: process.env.STATUS_REQUEST_URL || '',
			// port: process.env.STATUS_REQUEST_PORT ? process.env.STATUS_REQUEST_PORT : process.env.PORT || 80,
			timeout: process.env.STATUS_REQUEST_TIMEOUT || 5000,
			method: process.env.STATUS_REQUEST_METHOD || 'GET'
		},
		response: {
			status: process.env.REQUEST_RESPONSE_STATUS,
			html: process.env.REQUEST_RESPONSE_HTML || '',
			type: process.env.REQUEST_RESPONSE_TYPE || ''
		}
	},
	percentage: {
		memory: process.env.MEMORY_PERCENTAGE || 70,
		space: process.env.SPACE_PERCENTAGE || 70
	},
	codes: {
		message: [
			'The server is running',
			'The server is low in memory',
			'The server is low in space',
			'The request failed',
			'An unexpected error ocurred',
			'Server analysis is not supported',
			'Server is unreachable',
			'Server is unsecure (without ssl)',
			'Invalid server response status',
			'Server response with invalid HTML',
			'Invalid server response type'
		],
		values: {
			success: 0,
			error: {
				memory: 1,
				space: 2,
				request: 3,
				unexpected: 4,
				unsupported: 5,
				unreachable: 6,
				unsecure: 7,
				invalidResponseStatus: 8,
				invalidResponseHtml: 9,
				invalidResponseType: 10
			}
		}
	}
};

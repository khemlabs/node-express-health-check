# README

Node Express Health Check

## What is this repository for?

This library will analyze the space remaining on disk, the memory available
and perform a local request. With all that information it will create a response
object that can be used in a express middleware.

## methods

### statusMiddleware

This method can be used as an express middleware, that will add the status method
response to a **req.status** property.

### status

will analyze the space remaining on disk, the memory available and perform a
local request.

#### status response

{
httpCode:<200,406,500>, **200:success - 406:lib not implemented by server - 500:error**
status:<ok,warning,error>,
message:<string>
}

## Environment variables

- STATUS_REQUEST_PORT or PORT [optional] <number> default: **3001**
- STATUS_REQUEST_BASE_URL [optional] <string> default: **localhost**
- STATUS_REQUEST_TIMEOUT [optional] <string> default: **5000**
- STATUS_REQUEST_URL [optional] <string> default: **''**
- STATUS_REQUEST_METHOD [optional] <GET,POST,PATCH,DELETE> default: **GET**
- REQUEST_RESPONSE_STATUS [optional] <number> default: **200**
- REQUEST_RESPONSE_HTML [optional] <string> default: **''**
- REQUEST_RESPONSE_TYPE [optional] <json,html> default: **''**
- MEMORY_PERCENTAGE [optional] <number> default: **70**
- SPACE_PERCENTAGE [optional] <number> default: **70**

## TODO

ONLY WORKS WITH LINUX, update it to work with MACOS

## Who do I talk to?

- dnangelus repo owner and admin
- developer elgambet dev and admin

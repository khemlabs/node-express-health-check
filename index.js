const status = require('./lib/status');
const config = require('./config');

exports.statusMiddleware = async function(req, res, next) {
	try {
		const response = await status();
		req.status = response;
	} catch (error) {
		res.status = error;
	}
	next();
};

exports.status = status;

exports.codes = config.codes;

const { exec } = require('child_process');

module.exports = function(command) {
	return new Promise((resolve, reject) => {
		exec(command, (error, stdout, stderr) => {
			if (error) return reject({ error, stdout, stderr });
			return resolve({ stdout, stderr });
		});
	});
};

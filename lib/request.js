const axios = require('axios');
const config = require('../config');

async function request(customConfig = {}) {
	try {
		const baseURL = `http://${config.request.config.baseURL}`;
		const axiosConfig = { ...config.request.config, baseURL, ...customConfig };
		const response = await axios(axiosConfig);
		// Invalid status
		if (config.request.response.status && response.status !== config.request.response.status) {
			return config.codes.values.error.invalidResponseStatus;
		}
		// Invalid response type
		if (
			(config.request.response.type === 'html' && typeof response.data !== 'string') ||
			(config.request.response.type === 'json' && typeof response.data !== 'object')
		) {
			return config.codes.values.error.invalidResponseType;
		}
		// Invalid html
		if (
			config.request.response.type === 'html' &&
			config.request.response.html &&
			typeof response.data === 'string' &&
			response.data.indexOf(config.request.response.html)
		) {
			return config.codes.values.error.invalidResponseHtml;
		}
		// Success
		return config.codes.values.success;
	} catch (error) {
		return config.codes.values.error.unreachable;
	}
}

module.exports = async function(customConfig = {}) {
	try {
		const code = await request(customConfig);
		return code;
	} catch (error) {
		console.error(error);
		return config.codes.values.error.unreachable;
	}
};

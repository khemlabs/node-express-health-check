const exec = require('./exec');
const config = require('../config');
const request = require('./request');

const MEMORY_PERCENTAGE = `MEMORY_PERCENTAGE=${config.percentage.memory}`;
const SPACE_PERCENTAGE = `SPACE_PERCENTAGE=${config.percentage.space}`;
const SCRIPT_PATH = `${__dirname}/check.sh`;

const COMMAND = `${MEMORY_PERCENTAGE} ${SPACE_PERCENTAGE} sh ${SCRIPT_PATH}`;

const WARNING_CODES = [
	config.codes.values.error.memory,
	config.codes.values.error.space,
	config.codes.values.error.unsupported,
	config.codes.values.error.unsecure
];

function getErrorMessage(code) {
	return config.codes.message[code] || config.codes.message[config.codes.values.error.unexpected];
}

function getStatus(code) {
	if (code === config.codes.values.success) return 'ok';
	if (WARNING_CODES.includes(code)) return 'warning';
	return 'error';
}

function getHttpCode(code) {
	if (code === config.codes.values.success || code === config.codes.values.error.unsecure) return 200;
	if (code === config.codes.values.error.unsupported) return 406;
	return 500;
}

function getResponse(code) {
	const httpCode = getHttpCode(code);
	const status = getStatus(code);
	const message = getErrorMessage(code);
	return { httpCode, status, message };
}

module.exports = async function() {
	try {
		await exec(COMMAND);
		const requestStatus = await request();
		return getResponse(requestStatus);
	} catch (result) {
		const code = result && result.error && result.error.code ? result.error.code : result;
		return getResponse(code);
	}
};

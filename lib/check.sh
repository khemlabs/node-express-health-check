OS=`uname -s`

# test undefined error
#exit 10

if [ "$OS" = "Linux" ]; then
	MEMCHK=`/usr/bin/free | awk '/Mem/{printf("RAM Usage: %.0f\n"), $3/$2*100}' |  awk '{print $3}'`
	SPACECHK=`df --output=pcent -h -k / | sed '1d;s/[^0-9]//g'`

	if [ "$MEMCHK" -gt $MEMORY_PERCENTAGE ]; then
		exit 1
	fi

	if [ "$SPACECHK" -gt $SPACE_PERCENTAGE ]; then
		exit 2
	fi

	exit 0
else
	exit 5
fi